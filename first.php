<html>
    <head>
        <title> Topic Variable heading function </title>
    </head>
    <body>
        
        <?php
        
            echo "<h1> Topic Variable Heading Function </h1>";
            
            echo "<h2> Empty </h2>";
            
                $ruti = 0;

                if(empty($ruti))
                {
                    echo 'ruti is either 0, empty, or not set at all';
                }else{
                    echo 'Ruti is not 0';                }

                echo'<br/>';

                if(isset($ruti))
                {
                    echo 'Ruti is set even though it is empty' . $ruti;
                }
        
            echo"<h2> Gettype </h2>";
            
               $data = array(1, 1., NULL, new stdClass, 'foo');
               
               foreach($data as $datas)
                    {
                        echo gettype($datas) .'<br/>';
                    }
                        
            echo '<h2> is_array </h2>';
            
                $yes = array('This','is','an array');
                
                echo is_array($yes) ? 'Array' : 'not an array'; 
                
                echo '<br/>';

                $no = 'This is string';

                echo is_array($no) ? 'Array' : "Not an array ". $no ;
            
            echo '<h2> is_bool </h2>';
            
                $a = false;
                $b = 0;

                if(is_bool($a) === true)
                {
                    echo 'Yes, This is a boolean '. $a;
                }else{
                    echo 'No this in not boolean '. $a;
                }

                echo'<br/>';

                if(is_bool($b) === false)
                {
                    echo 'No, This is not boolean '.$b;
                }
                
             echo "<h2> is_end </h2>";
                
                
               
             echo '<h2> is_null </h2>';
             
                error_reporting(E_ALL);
                
                $foo = NULL;
                
                var_dump(is_null($foo)); 
                
             echo '<h2> is_object </h2>';
             
             
                function get_student($ami)
                {
                    if(!is_object($ami))
                    {
                        return false;
                    }
                    return $ami->student;
                }         

                $ami = new stdClass();
                $ami->student = array('ami','kisu','janina');


                var_dump(get_student(null));
                echo'<br/>';
                var_dump(get_student($ami));
             
             echo '<h2> is_string </h2>';
             
                $values = array(true, false, null, 'abc', '23', 23, '23.3', 23.3, '',' ', '0', 0);
             
                foreach($values as $value)
                {
                    echo "is_string(";
                    
                    var_export($value);
                    
                    echo") = "; 
                    
                    var_dump(is_string($value)); echo'<br/>';
                }
             
             echo '<h2> isset </h2>';
                
             $var=''; 
                
             if(isset($var))
             {
                 echo 'This var is set so I will print.';
             }
             
            
        
          echo '<h2> unset </h2>';
          
            function destory_foo()
            {
                global $foo;
                unset($foo);
            }
          
            $foo = 'bar';
            destory_foo();
            echo $foo;
            
           echo '<h2> vardump </h2>';
            
            $a = array('1','3',array('a','b','c'));
            
            var_dump($a);
            
            
                ?>
    </body>
</html>